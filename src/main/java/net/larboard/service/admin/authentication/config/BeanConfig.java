package net.larboard.service.admin.authentication.config;

import net.larboard.service.admin.authentication.service.AuthenticationTokenService;
import de.codecentric.boot.admin.server.web.client.HttpHeadersProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;

@Configuration
public class BeanConfig {
    private AuthenticationTokenService authenticationTokenService;

    @Autowired
    public BeanConfig(AuthenticationTokenService authenticationTokenService) {
        this.authenticationTokenService = authenticationTokenService;
    }

    @Bean
    public HttpHeadersProvider customHttpHeadersProvider() {
        return  instance -> {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer "+authenticationTokenService.getAuthToken());

            return httpHeaders;
        };
    }
}
